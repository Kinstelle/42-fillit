/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbruce <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/11 13:46:50 by cbruce            #+#    #+#             */
/*   Updated: 2019/01/07 14:18:57 by cmouele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

/*
** fill_map() browse each tetromino, beginning by the most top and left
** coordinates, to place them on the map. It then places it on the map after
** some verifications.
*/

static void	fill_map(t_list *list, t_map *map)
{
	t_tetromino *t;
	int pos_x;
	int pos_y;

	pos_x = 0;
	pos_y = 0;
	while (list)
    {
		t = (t_tetromino*)list->content;
    	clear_lines(t->str);
    	ft_putstr("exceed map ? ");
		ft_putnbr(exceed_map(t, map, pos_x, pos_y));
		ft_putchar('\n');
		ft_putchar('\n');
		ft_putstr("filled position ? ");
		ft_putnbr(filled_position(map->str, pos_x, pos_y));
		ft_putchar('\n');
		ft_putchar('\n');
		enlarge_map(map);
		ft_putchar('\n');
		ft_putchar('\n');
        list = list->next;
    }
}

/*
** map_str() constructs a char** with the width and height of the map that we
** calculated earlier.
*/

char	**map_str(int width, int height)
{
	char	**str_map;
	char	*line_map;
	int		x;
	int		y;

	str_map = (char**)malloc(sizeof(char*) * (width + 1));
	x = 0;
	y = 0;
	while (x < width)
	{
		y = 0;
		line_map = (char*)malloc(sizeof(char) * (height + 1));
		while (y < height)
		{
			line_map[y] = '.';
			y++;
		}
		line_map[y] = '\0';
		str_map[x] = line_map;
		x++;
	}
	str_map[x] = 0;
	return (str_map);
}

/*
** map_size() calculates the smaller map possible.
*/

static int	map_size(t_list *list)
{
	int	t;
	int	sharp;
	int	res;

	t = 0;
	sharp = 0;
	res = 0;
	while (list)
	{
		list = list->next;
		t++;
	}
	sharp = t * 4;
	res = ft_sqrt_rounded(sharp);
	return (res);
}

/*
** map() calls map_size() and map_str() to fill the map structure. Then it calls
** fill_map() to place each tetromino on the map.
*/

void		map(t_list *list)
{
	t_map	*map;

	map = (t_map*)malloc(sizeof(t_map));
	map->width = map_size(list);
	map->height = map_size(list);
	map->str = map_str(map->width, map->height);
	fill_map(list, map);
}
