/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cmouele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/07 09:01:33 by cmouele           #+#    #+#             */
/*   Updated: 2019/01/07 11:52:41 by cmouele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

/*
** to_letters() replaces each '#' by a letter.
*/

static void	to_letters(char **str_arr)
{
	static char	current_letter = 'A';
	int			line;
	int			column;

	line = 0;
	while (line < 4)
	{
		column = 0;
		while (column < 4)
		{
			if (str_arr[line][column] == '#')
				str_arr[line][column] = current_letter;
			column++;
		}
		line++;
	}
	current_letter++;
}

/*
** find_height() calculates the height of a tetromino.
*/

static int	find_height(char **str_arr)
{
	int	height;
	int	line;
	int	column;

	height = 0;
	line = 0;
	while (line < 4)
	{
		column = 0;
		while (column < 4)
		{
			if (str_arr[line][column] == '#')
			{
				height++;
				column = 3;
			}
			column++;
		}
		line++;
	}
	return (height);
}

/*
** find_width() calculates the width of a tetromino.
*/

static int	find_width(char **str_arr)
{
	int	width;
	int	line;
	int	column;

	width = 0;
	column = 0;
	while (column < 4)
	{
		line = 0;
		while (line < 4)
		{
			if (str_arr[line][column] == '#')
			{
				width++;
				line = 3;
			}
			line++;
		}
		column++;
	}
	return (width);
}

/*
** fill_str() fills a char ** for each *str.
*/

static char	**fill_str(char *str)
{
	int		x;
	int		y;
	size_t	c;
	char	**str_arr;
	char	*line;

	x = 0;
	c = 0;
	str_arr = (char**)malloc(sizeof(char*) * 5);
	while (x < 4)
	{
		y = 0;
		line = (char*)malloc(sizeof(char) * 5);
		while (y < 4 && c < ft_strlen(str))
		{
			line[y] = str[c];
			y++;
			c++;
		}
		line[y] = '\0';
		str_arr[x] = line;
		x++;
	}
	str_arr[x] = 0;
	return (str_arr);
}

/*
** fill_list() fills, for each str, a structure named tetromino, with the help
** of fill_str(), find_width() anh find_height(). Then it put them in a list.
*/

void		fill_list(char *str, t_list **list)
{
	t_tetromino	*tetromino;
	t_list		*element;

	tetromino = (t_tetromino*)malloc(sizeof(tetromino));
	tetromino->str = fill_str(str);
	tetromino->width = find_width(tetromino->str);
	tetromino->height = find_height(tetromino->str);
	to_letters(tetromino->str);
	element = ft_lstnew((void*)tetromino, sizeof(t_tetromino));
	ft_lstaddend(list, element);
}
