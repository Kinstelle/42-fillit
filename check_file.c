/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_file.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbruce <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/11 13:41:43 by cbruce            #+#    #+#             */
/*   Updated: 2019/01/07 09:26:23 by cmouele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

/*
** fill_str() checks if str is NULL. If it is, it duplicates line to put it in
** str. If not, it concatenates what is already in str with line.
*/

static int	fill_str(char *line, char **str)
{
	if (*str == NULL)
	{
		*str = ft_strdup(line);
		if (*str == NULL)
			return (0);
	}
	else
	{
		*str = ft_strjoin(*str, line);
		if (*str == NULL)
			return (0);
	}
	return (1);
}

/*
** For each string of tetromino, check_connect() checks if the is 6 or 8
** sides that touches. In these conditions, the tetromino is valid.
*/

static int	check_connect(char *str)
{
	int	connection;
	int	i;

	connection = 0;
	i = 0;
	while (i < 16)
	{
		if (str[i] == '#')
		{
			if ((i + 1) < 16 && str[i + 1] == '#')
				connection++;
			if ((i - 1) >= 0 && str[i - 1] == '#')
				connection++;
			if ((i + 4) < 16 && str[i + 4] == '#')
				connection++;
			if ((i - 4) >= 0 && str[i - 4] == '#')
				connection++;
		}
		i++;
	}
	if (connection != 6 && connection != 8)
		return (0);
	return (1);
}

/*
** For each string of tetromino, check_tetro() checks if there is 4 '#', 12 '.'
** and 16 characters. In these conditions, the tetromino is valid.
*/

static int	check_tetro(char *str)
{
	int	i;
	int	hash;
	int	point;

	i = 0;
	hash = 0;
	point = 0;
	while (str[i])
	{
		if (str[i] == '#')
			hash++;
		if (str[i] == '.')
			point++;
		i++;
	}
	if (hash != 4 || point != 12 || i != 16)
		return (0);
	return (1);
}

/*
** checks() calls check_tetro(), check_connect(), fill_list() to put the
** tetromino in a list, and reinitialize variables for the next tetromino.
*/

static int	checks(char *line, char **str, int *i, t_list **list)
{
	static int	tetro = 0;

	if (ft_strlen(line) || check_tetro(*str) != 1 || check_connect(*str) != 1)
		return (0);
	tetro = tetro + 1;
	if (tetro > 26)
		return (0);
	fill_list(*str, list);
	*str = NULL;
	*i = 0;
	return (1);
}

/*
** check_file() reads the file. For each line, it checks if there are 4
** characters and put the line in str. When is reaches a '\n', it calls
** checks().
*/

int			check_file(int fd, t_list **list)
{
	char	*line;
	char	*str;
	int		res;
	int		i;

	line = NULL;
	str = NULL;
	res = 1;
	i = 1;
	while (res == 1)
	{
		res = get_next_line(fd, &line);
		if (i % 5 == 0)
		{
			if (checks(line, &str, &i, list) != 1)
				return (0);
		}
		else
		{
			if (ft_strlen(line) != 4 || fill_str(line, &str) != 1)
				return (0);
		}
		i++;
	}
	return (1);
}
