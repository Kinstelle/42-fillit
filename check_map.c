/*
** Mettre le header de 42
*/

#include "fillit.h"

/*
** enlarge_map() is used to enlarge the map when all possibilities have been
** tested and do not fit the actual size of the map.
*/

void enlarge_map(t_map *map)
{
	ft_putstr("original map width: ");
	ft_putnbr(map->width);
	ft_putchar('\n');
	ft_putstr("original map height: ");
	ft_putnbr(map->height);
	ft_putchar('\n');
	map->width = map->width + 1;
	map->height = map->height + 1;
	map->str = map_str(map->width, map->height);
	ft_putstr("new map width: ");
	ft_putnbr(map->width);
	ft_putchar('\n');
	ft_putstr("new map height: ");
	ft_putnbr(map->height);
	ft_putchar('\n');
}

/*
** filled_position() checks if there already is a letter where we want to place
** the tetromino.
*/

int filled_position(char **map_str, int pos_x, int pos_y)
{
	if (map_str[pos_x][pos_y] != '.')
	{
		ft_putstr("letter already at this position\n");
		return (0);
	}
	return (1);
}

/*
** exceed_map() checks if the tetromino we want to place on the map on a
** certain coord won't exceed the map.
*/

int exceed_map(t_tetromino *t, t_map *map, int pos_x, int pos_y)
{
	if ((pos_x + t->width - 1) > map->width)
	{
		ft_putstr("exceed map width\n");
		return (0);
	}
	if ((pos_y + t->height - 1) > map->height)
	{
		ft_putstr("exceed map height\n");
		return (0);
	}
	return (1);
}

/*
** clear_columns() clears each tetromino of empty columns so that they can be
** at the left, fill new columns with '.' until they have 4 columns, and
** returns the cleared tetrominos.
*/

static char **clear_columns(char **tetro)
{
    int y;
    int z;

    y = 0;
    while (y < 4)
    {
		while (tetro[0][0] == '.' && tetro[1][0] == '.'
			   && tetro[2][0] == '.' && tetro[3][0] == '.')
		{
			z = 0;
			while (z < 4)
			{
				tetro[z][0] = tetro[z][1];
				tetro[z][1] = tetro[z][2];
				tetro[z][2] = tetro[z][3];
				tetro[z][3] = '.';
				z++;
			}
		}
        y++;
    }
    return (tetro);
}

/*
** clear_lines() clears each tetromino of empty lines so that they can be at
** the top, fill new lines with '.' until they have 4 lines, and then calls
** clear_columns().
*/

char **clear_lines(char **tetro)
{
	int x;

	x = 0;
	while (x < 4)
	{
		while (tetro[0][0] == '.' && tetro[0][1] == '.'
			   && tetro[0][2] == '.' && tetro[0][3] == '.')
		{
			ft_strcpy(tetro[0], tetro[1]);
			ft_strcpy(tetro[1], tetro[2]);
			ft_strcpy(tetro[2], tetro[3]);
			ft_strcpy(tetro[3], "....");
		}
		x++;
	}
	tetro = clear_columns(tetro);
	return (tetro);
}