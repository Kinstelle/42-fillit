/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cmouele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/05 13:07:35 by cmouele           #+#    #+#             */
/*   Updated: 2019/01/07 13:39:51 by cmouele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLIT_H
# define FILLIT_H

# define USAGE "usage: ./fillit file\n"
# define ERROR "error\n"

# include <fcntl.h>
# include "libft/libft.h"

typedef struct	s_tetromino
{
	int		width;
	int		height;
	char	**str;
}				t_tetromino;

typedef struct	s_map
{
	int		width;
	int		height;
	char	**str;
}				t_map;

int				check_file(int fd, t_list **list);
void			fill_list(char *str, t_list **list);
void			map(t_list *list);
char			**map_str(int width, int height);
char 			**clear_lines(char **tetro);
int				exceed_map(t_tetromino *t, t_map *map, int pos_x, int pos_y);
int				filled_position(char **map_str, int pos_x, int pos_y);
void			enlarge_map(t_map *map);

#endif
