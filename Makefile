#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: cmouele <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/12/07 13:06:13 by cmouele           #+#    #+#              #
#    Updated: 2018/12/11 13:55:27 by cbruce           ###   ########.fr        #
#                                                                              #
#******************************************************************************#

NAME = fillit
SRC = fillit.c check_file.c list.c map.c check_map.c
OBJ = $(SRC:.c=.o)
CC = gcc
CFLAGS = -Wall -Wextra -Werror

all: $(NAME)

$(NAME): $(OBJ)
	make -C libft
	$(CC) -o $(NAME) $(CFLAGS) $(SRC) -L libft -lft

clean:
	rm -rf $(OBJ)
	make -C libft clean

fclean: clean
	rm -rf $(NAME)
	make -C libft fclean

.PHONY: clean fclean
	make -C libft .PHONY

re:	fclean all
	make -C libft re
