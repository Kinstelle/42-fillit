/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cmouele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/05 13:09:56 by cmouele           #+#    #+#             */
/*   Updated: 2019/01/07 11:24:30 by cmouele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int	main(int argc, char **argv)
{
	int		open_res;
	t_list	*list;

	open_res = 0;
	list = NULL;
	if (argc != 2)
	{
		write(1, USAGE, ft_strlen(USAGE));
		exit(EXIT_FAILURE);
	}
	else
	{
		open_res = open(argv[1], O_RDONLY);
		if (open_res == -1)
			exit(EXIT_FAILURE);
		if (check_file(open_res, &list) != 1)
		{
			write(1, ERROR, ft_strlen(ERROR));
			exit(EXIT_FAILURE);
		}
		map(list);
		close(open_res);
	}
	return (0);
}
